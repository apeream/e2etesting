module.exports = { // adapted from: https://git.io/vodU0
  'Login correcto': function(browser) {
    browser
      .url('https://losestudiantes.co/')
      .click('.botonCerrar')
      .waitForElementVisible('.botonIngresar', 4000)
      .click('.botonIngresar')
      .waitForElementVisible('.cajaLogIn', 4000)
      .setValue('.cajaLogIn input[name="correo"]', 'pa2018@example.com')
      .setValue('.cajaLogIn input[name="password"]', 'foobar1234')
      .click('.cajaLogIn .logInButton')
      .waitForElementVisible('button[id="cuenta"]', 4000)
      .end();
  }
};