module.exports = { 
  'Creación de una cuenta con un login que ya existe': function(browser) {
    browser
      .url('https://losestudiantes.co/')
      .click('.botonCerrar')
      .waitForElementVisible('.botonIngresar', 4000)
      .click('.botonIngresar')
      .waitForElementVisible('.cajaSignUp', 4000)
      .setValue('.cajaSignUp input[name="nombre"]', 'Alexander')
      .setValue('.cajaSignUp input[name="apellido"]', 'Rios')
      .setValue('.cajaSignUp input[name="correo"]', 'pa2018@example.com')
      //.click('.cajaSignUp select[name="idUniversidad"] option[value="universidad-de-los-andes"]')
      .click('.cajaSignUp [type="checkbox"]')
      .click('.cajaSignUp select[name="idPrograma"] option[value="16"]')
      .setValue('.cajaSignUp input[name="password"]', 'foobar1234')
      .click('.cajaSignUp [type="checkbox"][name="acepta"]')
      .click('.cajaSignUp .logInButton')
      .waitForElementVisible('.sweet-alert', 4000)
      .assert.containsText('.sweet-alert', 'Ya existe un usuario registrado')
      .end();
  }
};