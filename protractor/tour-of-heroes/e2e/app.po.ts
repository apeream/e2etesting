import {browser, by, element, ElementFinder} from 'protractor';
import { forEach } from '@angular/router/src/utils/collection';

export class TourOfHeroesPage {
  navigateTo() {
    return browser.get('/');
  }

  getTop4Heroes() {
    return element.all(by.css('.module.hero')).all(by.tagName('h4')).getText();
  }

  navigateToHeroes() {
    element(by.linkText('Heroes')).click();
  }

  getAllHeroes() {
    return element(by.tagName('my-heroes')).all(by.tagName('li'));
  }

  enterNewHeroInInput(newHero: string) {
    element(by.tagName('input')).sendKeys(newHero);
    element(by.buttonText('Add')).click();
  }

  searchHeroe(name: string) {
    element(by.id('search-box')).sendKeys(name);
    element(by.css('.search-result')).click();
  }

  getDetailText() {
    return element(by.tagName('h2')).getText();
  }

  deleteHero(name: string) {
    var spanHero = element(by.xpath("//*[contains(text(), '" + name + "')]")).getWebElement();
    spanHero.getDriver().findElement(by.css('.delete')).click();
  }

  editHero(newName: string) {
    var input = element(by.css('input[placeholder="name"]'));
    input.clear().then(function() {
      input.sendKeys(newName);
    })
    element(by.xpath("//button[text()='Save']")).click();
  }

  navigateToHeroDashboard(name: string) {
    element(by.xpath("//h4[contains(text(), '" + name + "')]")).click();
  }

  navigateToHeroHeroes(name: string) {
    element(by.xpath("//*[contains(text(), '" + name + "')]")).element(by.xpath("..")).click(); 
    element(by.xpath("//button[text()='View Details']")).click();
  }
}
