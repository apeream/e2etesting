import { TourOfHeroesPage } from './app.po';

/* describe('Tour of heroes Dashboard', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage();
  });

  it('should display top 4 heroes', () => {
    page.navigateTo();
    expect(page.getTop4Heroes()).toEqual(['Mr. Nice', 'Narco', 'Bombasto', 'Celeritas']);
  });

  it('should navigate to heroes', () => {
    page.navigateToHeroes();
    expect(page.getAllHeroes().count()).toBe(11);
  });
});

describe('Tour of heroes, heroes page', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateToHeroes();
  });

  it('should add a new hero', () => {
    const currentHeroes = page.getAllHeroes().count();
    page.enterNewHeroInInput('My new Hero');
    expect(page.getAllHeroes().count()).toBe(currentHeroes.then(n => n + 1));
  });

});
 */
describe('Tour of heroes Dashboard', () => {
  let page: TourOfHeroesPage;
  const newHeroName = "My new hero"; 

  beforeEach(() => {
    page = new TourOfHeroesPage();
  });

  it('Search hero', () => {
    const name = 'Bombasto';
    page.navigateTo();
    page.searchHeroe(name);
    expect(page.getDetailText()).toEqual(name + ' details!');
  });

  it('delete hero', () => {
    page.navigateTo();
    page.navigateToHeroes();
    page.enterNewHeroInInput(newHeroName);
    page.navigateToHeroes();
    const currentHeroes = page.getAllHeroes().count();
    page.deleteHero(newHeroName);
    expect(page.getAllHeroes().count()).toBe(currentHeroes.then(n => n - 1));
  });

  it('Rename hero', () => {
    const name = 'Bombasto';
    const newName = 'Bombastic Fantastic';
    page.navigateTo();
    page.searchHeroe(name);
    page.editHero(newName);
    page.searchHeroe(newName);
    expect(page.getDetailText()).toEqual(newName + ' details!');
  });

  //Navegar a un héroe desde el dashboard
  it('Navigate to a hero from Dashboard', () => {
    const name = 'Narco';
    page.navigateTo();
    page.navigateToHeroDashboard(name);
    expect(page.getDetailText()).toEqual(name + ' details!');
  });

  //Navegar a un héroe desde la lista de héroes
  it('Navigate to a hero from Heroes', () => {
    const name = 'Narco';
    page.navigateTo();
    page.navigateToHeroes();
    page.navigateToHeroHeroes(name);
    expect(page.getDetailText()).toEqual(name + ' details!');
  });
  //Navegar a un héroe desde la búsqueda
   it('Navigate to a hero from Search', () => {
    const name = 'Narco';
    page.navigateTo();
    page.searchHeroe(name);
    expect(page.getDetailText()).toEqual(name + ' details!');
  });
});