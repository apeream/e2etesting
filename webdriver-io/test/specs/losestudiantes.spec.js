var assert = require('assert');
describe('los estudiantes login', function() {
    /* it('should visit los estudiantes and failed at log in', function () {
        browser.url('https://losestudiantes.co');
        browser.click('button=Cerrar');
        browser.waitForVisible('button=Ingresar', 5000);
        browser.click('button=Ingresar');

        var cajaLogIn = browser.element('.cajaLogIn');
        var mailInput = cajaLogIn.element('input[name="correo"]');

        mailInput.click();
        mailInput.keys('wrongemail@example.com');

        var passwordInput = cajaLogIn.element('input[name="password"]');

        passwordInput.click();
        passwordInput.keys('1234');

        cajaLogIn.element('button=Ingresar').click()
        browser.waitForVisible('.aviso.alert.alert-danger', 5000);

        var alertText = browser.element('.aviso.alert.alert-danger').getText();
        expect(alertText).toBe('Upss! El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.');
    }); */
    it('Dirigirse a la página de un profesor', function() {
        browser.url('https://losestudiantes.co/universidad-de-los-andes/ingenieria-de-sistemas/profesores/mario-linares-vasquez');
        browser.waitForVisible('.nombreProfesor', 5000);
        var teacherName = browser.element('.nombreProfesor').getText();
        expect(teacherName).toBe('Mario Linares Vasquez');
    });
    it('Filtros por materia en la página de un profesor', function() {
        browser.url('https://losestudiantes.co/universidad-de-los-andes/ingenieria-de-sistemas/profesores/mario-linares-vasquez')
        var estDatosCheck = browser.element('input[name="id:ISIS1206"]');
        estDatosCheck.click();
        browser.waitForVisible('.post', 5000);
        var liPost = browser.element('.post');
        var botonMasText = liPost.element('a').getText();
        expect(botonMasText).toMatch('Estructuras De Datos');
    });
});